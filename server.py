import asyncio

import numpy as np
from loguru import logger

import shiva as shv


async def manage_message_async(message: shv.ShivaMessage) -> shv.ShivaMessage:
    print("metadata:")
    print(list(message.metadata.keys()))

    print("tensors:")
    for tensor in message.tensors:
        print(tensor.shape, tensor.dtype)

    print("status:")
    print(message.tensors[0])

    return shv.ShivaMessage(metadata={}, tensors=[])


async def main_async():
    # Creates a Shive Server in Async mode
    server = shv.ShivaServerAsync(
        # this is the callback managing all incoming messages
        on_new_message_callback=manage_message_async,
        # this callback is useful to manage onConnect events
        on_new_connection=lambda x: logger.info(f"New connection -> {x}"),
        # this callback is useful to manage onDisconnect events
        on_connection_lost=lambda x: logger.error(f"Connection lost -> {x}"),
    )
    await server.wait_for_connections(forever=True, port=12345)


if __name__ == "__main__":
    asyncio.run(main_async())
