import multiprocessing
import typing as t

import objaverse
import objaverse.xl as objaversexl
import pipelime.items as pli
import pipelime.sequences as pls
import pydantic as pyd
import rich
from pipelime.commands.interfaces import OutputDatasetInterface
from pipelime.piper import PipelimeCommand, PiperPortType

import random


class ObjaverseDownloaderCommand(PipelimeCommand, title="objaverse-downloader"):
    output: OutputDatasetInterface = OutputDatasetInterface.pyd_field(
        alias="o",
        piper_port=PiperPortType.OUTPUT,
        description="Output folder",
    )
    tags: t.Optional[t.Sequence[str]] = pyd.Field(
        None,
        alias="t",
        description="Tags to filter objects, if `None` all objects are downloaded",
    )
    filter: t.Literal["or", "and"] = pyd.Field(
        "or",
        description="If `or` any given tag can match objects tags, \
                if `and` all given tags must match object tags",
    )
    comparison: t.Literal["contains", "equals"] = pyd.Field(
        "contains",
        description="if `contains` object tag match if it contains the given tag, \
                if `equals` object tag match if it is equal to the given tag",
    )
    max_objects: t.Optional[int] = pyd.Field(
        None, gt=0, description="Maximum number of objects to download"
    )
    random: bool = pyd.Field(
        False,
        description="Whether to download the first `max_objects` random objects or not",
    )
    object_key: str = pyd.Field("mesh", description="Output object key")
    metadata_key: str = pyd.Field("metadata", description="Output metadata key")

    def run(self):
        # Load annotations
        rich.print("[green]Loading annotations...[/green]")
        object_metadata = objaverse.load_annotations()
        # Filter uids by tags
        if self.tags is None:
            uids = list(object_metadata.keys())
        else:
            uids = [
                uid
                for uid, annotation in object_metadata.items()
                if (any if self.filter == "or" else all)(
                    [
                        any(
                            [
                                x.lower() in y["name"].lower()
                                if self.comparison == "contains"
                                else x.lower() == y["name"].lower()
                                for y in annotation["tags"]
                            ]
                        )
                        for x in self.tags
                    ]
                )
            ]
        # Create output stream
        max_objects = self.max_objects if self.max_objects is not None else len(uids)
        if self.random:
            random.shuffle(uids)
        uids = uids[:max_objects]
        stream = pls.DataStream(output_pipe=self.output.as_pipe())
        # Load objects
        rich.print("[green]Loading objects...[/green]")
        objects_path = objaverse.load_objects(
            uids, download_processes=multiprocessing.cpu_count()
        )
        for idx, uid in enumerate(self.track(uids, message="Writing samples...")):
            # Load object path
            object_path = objects_path[uid]
            # Load object metadata
            object_metadata = objaverse.load_annotations(uids=[uid])[uid]
            # Create object item
            object_item = pli.Item.get_instance(object_path)
            # Create metadata item
            metadata_item = pli.YamlMetadataItem(object_metadata)
            # Create sample
            data = {self.object_key: object_item, self.metadata_key: metadata_item}
            sample = pls.Sample(data=data)
            # Write sample
            stream.set_output(idx, sample)


# class ObjaverseXLDownloaderCommand(PipelimeCommand, title="objaversexl-downloader"):
#     output: OutputDatasetInterface = OutputDatasetInterface.pyd_field(
#         alias="o",
#         piper_port=PiperPortType.OUTPUT,
#         description="Output folder",
#     )
#     max_objects: t.Optional[int] = pyd.Field(
#         None, gt=0, description="Maximum number of objects to download"
#     )
#     random: bool = pyd.Field(
#         False,
#         description="Whether to download the first `max_objects` random objects or not",
#     )
#     object_key: str = pyd.Field("mesh", description="Output object key")
#     metadata_key: str = pyd.Field("metadata", description="Output metadata key")

#     def run(self):
#         objects = objaversexl.get_annotations()
#         indices = list(range(0, len(objects)))
#         if self.random:
#             random.shuffle(indices)
#         if self.max_objects:
#             indices = indices[: self.max_objects]

#         # get the rows from the dataframe using indices
#         objects = objects.iloc[indices]
#         fid2path = objaversexl.download_objects(objects)
#         print(indices)
#         print(fid2path)

#         # WRONG, fid2path can be smaller than indices
#         stream = pls.DataStream(output_pipe=self.output.as_pipe())
#         for i, (fid, path) in enumerate(fid2path.items()):
#             object_item = pli.Item.get_instance(path)
#             idx = indices[i]
#             metadata = objects.iloc[idx]
#             print(fid)
#             print(metadata)
#             # metadata_item = pli.YamlMetadataItem(object_metadata)
#             # sample = pls.Sample(
#             #     {self.object_key: object_item, self.metadata_key: metadata_item}
#             # )
#             # stream.set_output(idx, sample)
