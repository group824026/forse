from pipelime.sequences import Sample
from pipelime.stages import SampleStage
import genesys.neural_twin


class SetMeshStage(SampleStage, title="set-mesh"):
    def __call__(self, x: Sample) -> Sample:
        neural_twin = x["neural_twin"]()
        mesh = x["mesh"]()
        neural_twin.mesh = mesh
        x = x.set_value_as("neural_twin", "neural_twin", neural_twin)
        return x


class ShowMeshStage(SampleStage, title="show-mesh"):
    def __call__(self, x: Sample) -> Sample:
        x["neural_twin"]().mesh.show()
        return x


class PrintMetadataStage(SampleStage, title="print-meta"):
    def __call__(self, x: Sample) -> Sample:
        print(x["neural_twin"]().metadata)
        return x
