import tempfile
from pathlib import Path

import bpy
import numpy as np
import pipelime.sequences as pls
import trimesh
import yaml
from rich.progress import track


class Shapenet2Underfolder:
    @classmethod
    def convert(cls, input_folder, output_folder):
        input_folder = Path(input_folder)
        output_folder = Path(output_folder)
        categories = sorted([x for x in input_folder.glob("*") if x.is_dir()])
        objects = [x for y in categories for x in sorted(y.glob("*")) if x.is_dir()]
        objects = [x / "models" / "model_normalized.obj" for x in objects][:10]
        zfill = len(str(len(objects) - 1))
        output_folder_ = output_folder / "data"
        output_folder_.mkdir(parents=True, exist_ok=True)
        for i, obj in enumerate(track(objects)):
            cls._convert(obj, output_folder_ / f"{str(i).zfill(zfill)}_model.glb")

    @classmethod
    def _convert(cls, input_path: Path, output_path: Path):
        with tempfile.TemporaryDirectory() as tmpdir:
            tmppath = Path(tmpdir) / "model.glb"
            bpy.ops.object.select_all(action="SELECT")
            bpy.ops.object.delete()
            bpy.ops.wm.obj_import(
                filepath=str(input_path), forward_axis="Y", up_axis="Z"
            )
            bpy.ops.transform.rotate(value=np.pi / 2, orient_axis="X")
            bpy.ops.object.transform_apply(rotation=True)
            # bpy.ops.object.editmode_toggle()
            # bpy.ops.mesh.select_mode(type="FACE")
            # bpy.ops.mesh.select_all(action="SELECT")
            # bpy.ops.mesh.set_normals_from_faces()
            # bpy.ops.object.editmode_toggle()
            bpy.ops.export_scene.gltf(filepath=str(tmppath), export_format="GLB")
            bpy.ops.object.editmode_toggle()
            bpy.ops.object.editmode_toggle()
            bpy.ops.export_scene.gltf(filepath=str(output_path), export_format="GLB")
            yaml.safe_dump(
                {"input_path": str(input_path)},
                open(output_path.parent / f"{output_path.stem}_source.yml", "w"),
            )


# Shapenet2Underfolder.convert(
#     "/media/federico/SlowStorage/backup/federico_utils/ShapeNetCore.v2_fai4ba",
#     "ShapeNetCore.v2_fai4ba_uf_4",
# )

# seq = pls.SamplesSequence.from_underfolder("ShapeNetCore.v2_fai4ba_uf_4")
# for sample in seq:
#     mesh = sample["model"]()
#     mesh.show()
