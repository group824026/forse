import eyenerf.neural_twin as ntwin
import numpy as np
import open3d as o3d
import pipelime.sequences as pls
import trimesh


class Keypoints3DLabeler:
    DT = 0.001
    DS = 0.1

    def __init__(self, mesh: trimesh.Trimesh):
        self.mesh = mesh
        self.mesh_o3d = mesh.as_open3d
        self.mesh_o3d = self.mesh_o3d.compute_vertex_normals()
        self.initial_translation = self.mesh.center_mass
        self.initial_scale = 0.001
        self.spheres = []
        self.translations = []
        self.scales = []

    def _add_point(self, vis: o3d.visualization.Visualizer) -> bool:
        self.translations.append(self.initial_translation.copy())
        self.scales.append(self.initial_scale)
        sphere = o3d.geometry.TriangleMesh.create_sphere(radius=self.scales[-1])
        sphere.paint_uniform_color([0.1, 0.1, 0.7])
        sphere = sphere.compute_vertex_normals()
        sphere.translate(self.translations[-1], relative=False)
        vis.add_geometry(sphere, reset_bounding_box=False)
        self.spheres.append(sphere)

    def _translate_x_plus(self, vis: o3d.visualization.Visualizer) -> bool:
        sphere = self.spheres[-1]
        self.translations[-1] += np.array([self.DT, 0.0, 0.0])
        sphere.translate(self.translations[-1], relative=False)
        vis.update_geometry(sphere)

    def _translate_x_minus(self, vis: o3d.visualization.Visualizer) -> bool:
        sphere = self.spheres[-1]
        self.translations[-1] += np.array([-self.DT, 0.0, 0.0])
        sphere.translate(self.translations[-1], relative=False)
        vis.update_geometry(sphere)

    def _translate_y_plus(self, vis: o3d.visualization.Visualizer) -> bool:
        sphere = self.spheres[-1]
        self.translations[-1] += np.array([0.0, self.DT, 0.0])
        sphere.translate(self.translations[-1], relative=False)
        vis.update_geometry(sphere)

    def _translate_y_minus(self, vis: o3d.visualization.Visualizer) -> bool:
        sphere = self.spheres[-1]
        self.translations[-1] += np.array([0.0, -self.DT, 0.0])
        sphere.translate(self.translations[-1], relative=False)
        vis.update_geometry(sphere)

    def _translate_z_plus(self, vis: o3d.visualization.Visualizer) -> bool:
        sphere = self.spheres[-1]
        self.translations[-1] += np.array([0.0, 0.0, self.DT])
        sphere.translate(self.translations[-1], relative=False)
        vis.update_geometry(sphere)

    def _translate_z_minus(self, vis: o3d.visualization.Visualizer) -> bool:
        sphere = self.spheres[-1]
        self.translations[-1] += np.array([0.0, 0.0, -self.DT])
        sphere.translate(self.translations[-1], relative=False)
        vis.update_geometry(sphere)

    # def _scale_plus(self, vis: o3d.visualization.Visualizer) -> bool:
    #     sphere = self.spheres[-1]
    #     self.scales[-1] *= self.DS
    #     sphere.scale(self.scales[-1], self.translations[-1])
    #     vis.update_geometry(sphere)

    # def _scale_minus(self, vis: o3d.visualization.Visualizer) -> bool:
    #     sphere = self.spheres[-1]
    #     self.scales[-1] *= 1 / self.DS
    #     sphere.scale(self.scales[-1], self.translations[-1])
    #     vis.update_geometry(sphere)

    def label(self):
        cbs = {
            ord("Z"): self._add_point,
            ord("W"): self._translate_x_plus,
            ord("S"): self._translate_x_minus,
            ord("D"): self._translate_y_plus,
            ord("A"): self._translate_y_minus,
            ord("R"): self._translate_z_plus,
            ord("F"): self._translate_z_minus,
            # ord("T"): self._scale_plus,
            # ord("G"): self._scale_minus,
        }
        o3d.visualization.draw_geometries_with_key_callbacks([self.mesh_o3d], cbs)


sseq = pls.SamplesSequence.from_underfolder("/home/federico/Music/meshes")
for sample in sseq:
    nt = sample["neural_twin"]()
    mesh = nt.mesh
    labeler = Keypoints3DLabeler(mesh)
    labeler.label()
